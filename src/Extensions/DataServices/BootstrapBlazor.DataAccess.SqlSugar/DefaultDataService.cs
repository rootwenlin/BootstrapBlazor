﻿using BootstrapBlazor.Components;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;
using System.Reflection;

namespace BootstrapBlazor.DataAccess.SqlSugar;


/// <summary>
/// FreeSql ORM 的 IDataService 接口实现
/// </summary>
/// <remarks>
/// 构造函数
/// </remarks>
class DefaultDataService<TModel>(ISqlSugarClient db) : DataServiceBase<TModel> where TModel : class, new()
{
    /// <summary>
    /// 删除方法
    /// </summary>
    /// <param name="models"></param>
    /// <returns></returns>
    public override async Task<bool> DeleteAsync(IEnumerable<TModel> models)
    {
        // 通过模型获取主键列数据
        // 支持批量删除
        await db.Deleteable<TModel>(models).ExecuteCommandAsync();
        return true;
    }

    /// <summary>
    /// 保存方法
    /// </summary>
    /// <param name="model"></param>
    /// <param name="changedType"></param>
    /// <returns></returns>
    public override async Task<bool> SaveAsync(TModel model, ItemChangedType changedType)
    {
        await db.Storageable(model).ExecuteCommandAsync();
        return true;
    }

    /// <summary>
    /// 查询方法
    /// </summary>
    /// <param name="option"></param>
    /// <returns></returns>
    public override Task<QueryData<TModel>> QueryAsync(QueryPageOptions option)
    {
        int count = 0;

        var filter = option.ToFilter();

        var items = db.Queryable<TModel>()
            .WhereIF(filter.HasFilters(), filter.GetFilterLambda<TModel>())
            .OrderByIF(option.SortOrder != SortOrder.Unset, $" {option.SortName} {option.SortOrder}")
            .ToPageList(option.PageIndex, option.PageItems, ref count);

        var ret = new QueryData<TModel>()
        {
            TotalCount = count,
            Items = items,
            IsSorted = option.SortOrder != SortOrder.Unset,
            IsFiltered = option.Filters.Any(),
            IsAdvanceSearch = option.AdvanceSearches.Any(),
            IsSearch = option.Searches.Any() || option.CustomerSearches.Any()
        };
        return Task.FromResult(ret);
    }
}
